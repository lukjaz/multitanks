import Settings from '../../Settings';
import Position from '../Position';
import Size from '../Size';

/*
  Klasa reprezentująca model bazowy stanu obiektu gry.
*/
export default class ObjectState {

  constructor(color, size, position, speed, angle) {
    this.size = size || Settings.playerSize;
    this.position = position || Settings.playerPosition;
    this.speed = speed || Settings.playerSpeed;
    this.angle = angle || Settings.playerAngle;
    this.color = color;
  }

  /*
    Metoda dzięki której możliwe jest kopiowanie zawartości modelu, bez kopiowania referencji na niego.
  */
  clone() {
    return new ObjectState(this.color, this.size.clone(), this.position.clone(), this.speed, this.angle);
  }

  /*
    Metoda używana przy odtwarzaniu obiektu stanu po przesłaniu go z serwera do klienta.
  */
  static restoreState(objectState) {
    return new ObjectState(
      objectState.color,
      new Size(objectState.size.width, objectState.size.height),
      new Position(objectState.position.x, objectState.position.y),
      objectState.speed,
      objectState.angle
    );
  }
}
