import socketio from 'socket.io';
import Queue from '../shared/Collections/Queue';
import TimedQueue from '../shared/Collections/TimedQueue';
import Message from './Models/Message';
import Client from './Models/Client';
import ServerData from '../shared/Models/ServerData';
import RoomManager from './RoomManager';
import EndingStatus from '../shared/Models/EndingStatus';

/*
  Klasa zajmująca się komunikacją z klientami, oraz zarządzająca wiadomościami otrzymanymi od nich.
  Ważne pola:
    -roomManager - referencja na RoomManager
    -gameManager - referencja na GameManager
    -messages - kolejka wszystkich nieprzetworzonych wiadomości od klientów
*/
export default class ConnectionService {

  constructor(gameManager, httpServer, roomManager) {
    this.io = socketio(httpServer);
    this.roomManager = roomManager;
    this.messages = new Queue();
    this.gameManager = gameManager;
  }

  listen() {
    //zainicjowanie połączenia z klientem
    this.io.on('connection', (socket) => {
      this.onClientConnected(socket);

      //nasłuchiwanie na nowe stany wysyłane przez klientów
      socket.on('state changed', (messageModel) => {
        this.onMessageArrived(messageModel, socket);
      })

      //obsługa zdarzenia rozłączenia klienta z serwerem
      socket.on('disconnect', () => {
        this.onClientDisconnected(socket);
      })
    });
  }

  /*
    Metoda obsługująca zdarzenie połączenia klienta z serwerem. Gdy tylko takie nastąpi
    łączący się klient otrzymuje informację o nawiązaniu połączenia.
    Następnie inicjowany jest jego serwerowy model i dodawany do kolejki klientów oczekujących na pokój.
    Jeśli dodanie tego klienta stworzyło pokój, to metoda rozsyła informację o tym zdarzeniu
    wszystkim klientom z tego pokoju.
    Parametry:
      -socket - gniazdo klienta, który nawiązał połączenie
  */
  onClientConnected(socket) {
    console.log(`user connected: ${socket.id}`);
    socket.emit('connection');
    const states = new TimedQueue(2000);
    const room = this.roomManager.addPlayer(new Client(socket, states));
    if(room) {
      let i = 0;
      room.players.forEach((player) => {
        const initialState = this.gameManager.getInitialState(i++); //nadawanie początkowego stanu wszystkim klientom z pokoju
        player.states.add(initialState);
        player.socket.join(room.id); //dołączenie socketu do pokoju w Socket.io
        player.socket.emit('game started', new ServerData(player.socket.id, initialState));
      });
    }
  }

  /*
    Metoda obsługująca wiadomości nadsyłane od klientów. Są one dodawane do kolekcji nieprzetworzonych wiadomości.
    Parametry:
      -messageContent - zawartość odebranej wiadomości
      -socket - gniazdko klienta, który wysłał wiadomość
  */
  onMessageArrived(messageContent, socket) {
    const time = Date.now();
    this.messages.add(new Message(messageContent, socket.id, time));
  }

  /*
    Metoda obsługująca rozłączenie klienta z serwerem.
    Klient usuwany jest z pokoju i jeśli jego usunięcie spowodowało koniec gry
    reszta klientów z pokoju dostaje o tym informację.
    Parametry:
      -socket - gniazdo klienta, który rozłączył się z serwerem
  */
  onClientDisconnected(socket) {
    const id = socket.id;
    const { players, gameEnded, roomId } = this.roomManager.deletePlayer(id);
    if(gameEnded && players) {
      this.io.to(roomId).emit('game ended', EndingStatus.PLAYER_DISCONNECTED);
      players.forEach((player) => {
        player.socket.leave(roomId);
        this.onClientConnected(player.socket);
      });
    }
    if(roomId !== null) {
      this.io.to(roomId).emit('user disconnected', id);
    }
    console.log(`user disconnected: ${id}`);
  }

  /*
    Metoda rozsyłająca do wszystkich klientów stany klientów i pocisków z tych samych pokoi.
  */
  notifyClients() {
    this.roomManager.rooms.forEach((room, roomId) => {
      const playersStates = [];
      const bulletsArray = [];
      room.players.forEach((player, socketId) => {
        const newState = player.states.newest();
        if(newState) {
          playersStates.push(new ServerData(socketId, newState));
        }
      });
      room.bullets.forEach((bulletState, id) => {
        bulletsArray.push({bulletState: bulletState, id: id});
      });
      this.io.to(roomId).emit('new state', {playersStates: playersStates, bullets: bulletsArray});
    });
  }

  /*
    Metoda obsługująca usuwanie przegranego klienta z pokoju gry.
    Przegrany dostaje informację o swoim stanie ze statusem DEFEAT
    Jeśli jego porażka zakończyła grę, reszta graczy z pokoju otrzymuje informację o wygranej
    W przeciwnym wypadku reszta graczy z pokoju dostaje informację o rozłączeniu przeciwnika.
    Parametry:
      -socketId - id gniazda usuwanego klienta
  */
  removeDefeatedPlayer(socketId) {
    const roomPlayer = this.roomManager.getPlayerData(socketId);
    if(roomPlayer.player) {
      roomPlayer.player.socket.emit('game ended', EndingStatus.DEFEAT);
      roomPlayer.player.socket.leave(roomId);
    }
    const {players, gameEnded, roomId} = this.roomManager.deletePlayer(socketId);
    if(gameEnded && players) {
      players.forEach((player) => {
        player.socket.emit('game ended', EndingStatus.WIN);
        player.socket.leave(roomId);
      });
    }
    if(roomId !== null) {
      this.io.to(roomId).emit('user disconnected', socketId);
    }
  }

  getMessages() {
    return this.messages;
  }
}
