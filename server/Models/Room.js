/*
  Klasa reprezentująca pokój gry.
  Ważne pola:
    -id - id pokoju
    -players - kolekcja obiektów klasy Client należąca do pokoju
    -bullets - kolekcja pocisków klasy BulletState należących do pokoju
*/
export default class Room {
  constructor(id, players, bullets) {
    this.id = id;
    this.players = new Map();
    players.forEach((player) => {
      this.players.set(player.socket.id, player);
    });
    this.bullets = bullets || new Map();
  }
}
