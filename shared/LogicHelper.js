/*
  Klasa pomocnicza, mapująca modele używane na kliencie oraz na serwerze na modele akceptowane przez LogicManager.
*/
export default class LogicHelper {

  static getLogicPlayersForClientEntities(entities, userId) {
    const result = [];
    entities.forEach((entity, id) => {
      if(!userId || id !== userId) {
        const state = entity.object.getState();
        result.push({position: state.position.clone(), health: state.health, size: state.size.clone(), id: id});
      }
    });
    return result;
  }

  static getLogicPlayersForServerRoom(room, userId) {
    const result = [];
    room.players.forEach((player, id) => {
      if(!userId || id !== userId) {
        const state = player.states.newest().objectState;
        result.push({position: state.position.clone(), health: state.health, size: state.size.clone(), id: id});
      }
    });
    return result;
  }
}
