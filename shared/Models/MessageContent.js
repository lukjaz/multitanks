export default class MessageContent {

  constructor(inputState, historyId, timeDiff) {
    this.inputState = inputState;
    this.historyId = historyId;
    this.timeDiff = timeDiff;
  }
}
