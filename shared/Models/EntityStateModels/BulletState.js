import ObjectState from './ObjectState';

export default class BulletState extends ObjectState {

  constructor(objectState, userId, historyId) {
    super(objectState.color, objectState.size, objectState.position, objectState.speed, objectState.angle);
    this.userId = userId;
    this.historyId = historyId;
  }

  clone() {
    const baseResult = super.clone();
    return new BulletState(baseResult, this.userId, this.historyId);
  }

  static restoreState(bulletState) {
    const restoredBase = ObjectState.restoreState(bulletState);
    return new BulletState(restoredBase, bulletState.userId, bulletState.historyId);
  }
}
