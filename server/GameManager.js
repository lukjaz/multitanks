import ConnectionService from './ConnectionService';
import LogicManager from '../shared/LogicManager';
import Position from '../shared/Models/Position';
import PlayerState from '../shared/Models/EntityStateModels/PlayerState';
import ObjectState from '../shared/Models/EntityStateModels/ObjectState';
import LogicModel from '../shared/Models/LogicModel';
import ClientData from '../shared/Models/ClientData';
import Settings from '../shared/Settings';
import LogicHelper from '../shared/LogicHelper';
import RoomManager from './RoomManager';

/*
  Klasa odpowiadająca za obsługę gry po stronie serwera. Używa w tym celu dwóch pętli:
  pętli przetwarzania wiadomości od klientów, oraz pętli rozsyłania nowych stanów do klientów.
*/
export default class GameManager {

  constructor(httpServer) {
    this.roomManager = new RoomManager();
    this.connectionService = new ConnectionService(this, httpServer, this.roomManager);
    this.logicManager = new LogicManager();
  }

  /*
    Metoda uruchamiająca dwie główne pętle życia gry po stronie serwera
  */
  run() {
    this.connectionService.listen();
    this.setupMessageLoop();
    this.setupBroadcastLoop();
  }

  /*
    Pętla obsługi wiadomości od klientów. Zamienia kolejne wiadomości na nowe stany modeli klientów.
    Dodatkowo obsługuje obliczanie nowych stanów pocisków na podstawie różnic czasowych między kolejnymi
    wywołaniami pętli.
  */
  setupMessageLoop() {
    let time = null;
    setInterval(() => {
      const currentTime = Date.now();
      const previousTime = time || currentTime;
      const timeDiff = currentTime - previousTime;
      time = currentTime;
      this.processMessages();
      this.updateBullets(timeDiff / 100); //użycie różnicy czasowej w milisekundach
    }, Settings.serverMessageStep)
  }

  setupBroadcastLoop() {
    setInterval(() => {
      this.connectionService.notifyClients();
    }, Settings.serverUpdateStep)
  }

  /*
    Metoda tworząca początkowy stan dla kolejnych klientów w pokoju. Obecnie obsługuje nadawanie stanu dla maksymalnie 3 klientów.
    Parametry:
      -index - indeks oznaczający kolejność gracza w kolejce oczekiwania
  */
  getInitialState(index) {
    const initialStates = [
      new ClientData(new PlayerState(new ObjectState('green', null, new Position(0, 0), null, 180))),
      new ClientData(new PlayerState(new ObjectState('red', null,
        new Position(Settings.canvasSize.width - Settings.playerSize.width, Settings.canvasSize.height - Settings.playerSize.height),
        null, 0)))
    ];
    return initialStates[index] || new ClientData(new PlayerState(new ObjectState('blue')));
  }

  /*
    Metoda przetwarzająca wiadomości od klientów, które zostały nadesłane przed jej wywołaniem.
    Dla każdej wiadomości pobiera ostatni przetworzony stan klienta i odczytuje z wiadomości stan wciśniętych klawiszy, oraz różnicę czasową.
    Następnie odczytane informacje aplikowane są do LogicManagera, a rezultat jego obliczeń jest zapisywany jako najnowszy stan danego klienta.
    Jeśli LogicManager zwróci informację o potrzebie stworzenia pocisku, to jest on natychmiast dodawany do odpowiedniego pokoju gry.
  */
  processMessages() {
    const currentTime = Date.now();
    const messages = this.connectionService.getMessages();
    while(!messages.isEmpty()){
      const message = messages.pop();
      if(message.time > currentTime) { //przetwarzanie wiadomości nadesłanych z przed wywołania metody
        break;
      }
      const socketId = message.socketId;
      const messageContent = message.content;
      const roomPlayerData = this.roomManager.getPlayerData(socketId);
      //dodatkowe zabezpieczenie przed oszukiwaniem - metoda nie analizuje stanów klienta, których renderowanie trwało za długo
      //(prawdopodobna ingerencja w kod źródłowy z poziomu przeglądarki)
      if(!roomPlayerData || messageContent.timeDiff > 100) {
        break;
      }
      let latestServerState = roomPlayerData.player.states.newest();
      if(!latestServerState) {
        break;
      }
      const otherPlayers = LogicHelper.getLogicPlayersForServerRoom(this.roomManager.getRoom(roomPlayerData.roomId), socketId);
      const { bulletState, playerState } = this.logicManager.processPlayerLogic(new LogicModel(messageContent.inputState, latestServerState.objectState, messageContent.timeDiff, otherPlayers));
      roomPlayerData.player.states.add(new ClientData(playerState, messageContent.timeDiff, messageContent.historyId));
      if(bulletState) {
        bulletState.userId = message.socketId;
        bulletState.historyId = messageContent.historyId;
        this.roomManager.addBullet(bulletState, roomPlayerData.roomId);
      }
    }
  }

  /*
    Metoda sprawdzająca, czy gracz powinien przegrać.
    Parametry:
      -playerState - stan sprawdzanego gracza
      -socketId - id gniazda sprawdzanego gracza
  */
  serveDefeatConditions(playerState, socketId) {
    if(playerState.health === 0) {
      this.connectionService.removeDefeatedPlayer(socketId)
    }
  }

  /*
    Metoda aktualizująca stan wszystkich pocisków dla każdego z pokoi.
    LogicManager wylicza dla każdego z pocisków jego nowy stan na podstawie stanu poprzedniego,
    różnicy czasowej od ostatnich obliczeń, oraz pozycji pojazdów użytkowników.
    Jeśli z obliczeń LogicManagera wyniknie, że pocisk wyleciał poza ekran, lub zderzył się z pojazdem, to pocisk zostaje usunięty.
    Jeśli zostanie wykryta kolizja z pojazdem to dodatkowo zostaje aktualizowana wartość życia dla danego klienta, oraz sprawdzane są warunki zwycięstwa.
    Parametry:
      -timeDiff - różnica czasu od ostatniego wywołania pętli analizy wiadomości
  */
  updateBullets(timeDiff) {
    this.roomManager.rooms.forEach((room, roomId) => {
      const playerStates = LogicHelper.getLogicPlayersForServerRoom(this.roomManager.getRoom(roomId));
      const bulletsToRemove = [];
      room.bullets.forEach((oldBulletState, id) => {
        const result = this.logicManager.processBulletLogic(new LogicModel(null, oldBulletState, timeDiff, playerStates));
        if(!result || result.collisionDetected) {
          bulletsToRemove.push(id);
        } else {
          this.roomManager.updateBullet(id, roomId, result.bulletState);
        }
        if(result && result.collisionDetected) {
          const damagedRoomPlayer = this.roomManager.getPlayerData(result.playerState.userId);
          if(damagedRoomPlayer) {
            const newest = damagedRoomPlayer.player.states.newest();
            newest.objectState.health = result.playerState.health;
            this.serveDefeatConditions(newest.objectState, damagedRoomPlayer.player.socket.id);
          }
        }
      });
      bulletsToRemove.forEach((id) => {
        this.roomManager.deleteBullet(id, roomId);
      });
    });
  }
}
