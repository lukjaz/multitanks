import Position from './Models/Position';
import Size from './Models/Size';

const settings = {
  canvasSize: new Size(600, 600),
  playerSize:new Size(40, 40),
  playerCooldown: 10,
  playerPosition: new Position(100, 100),
  serverUpdateStep: 45,
  serverMessageStep: 16,
  lag: 100,
  playerSpeed: 15,
  bulletSpeed: 30,
  playerAngle: 0,
  health: 200,
  roomSize: 2,
  interpolationEnabled: true,
  clientPredictionEnabled: true
};

export default settings;
