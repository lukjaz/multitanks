/*
  Klasa reprezentująca model klienta na serwerze.
  Ważne pola:
    -socket - gniazdo klienta
    -states - reprezentuje kolejkę czasową przeszłych stanów pojazdu klienta.
      Dzięki przetrzymywaniu tych ubiegłych stanów, a nie tylko ostatniego, możliwe jest użycie
      techniki Lag Compensation - (jeśli byłaby w przyszłości potrzebna)
*/
export default class Client {

  constructor(socket, states) {
    this.socket = socket;
    this.states = states;
  }
}
