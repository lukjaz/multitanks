import InputManager from './InputManager';
import InputState from '../../shared/Models/InputState';
import LogicManager from '../../shared/LogicManager';
import BulletManager from './BulletManager';
import Connector from './Connector';
import Player from './Entities/Player';
import PlayerState from '../../shared/Models/EntityStateModels/PlayerState';
import ObjectState from '../../shared/Models/EntityStateModels/ObjectState';
import BulletState from '../../shared/Models/EntityStateModels/BulletState';
import MessageContent from '../../shared/Models/MessageContent';
import LogicModel from '../../shared/Models/LogicModel';
import GameEntity from './Entities/GameEntity';
import Position from '../../shared/Models/Position';
import ClientData from '../../shared/Models/ClientData';
import Bullet from './Entities/Bullet';
import HistoryModel from './Models/HistoryModel';
import LogicHelper from '../../shared/LogicHelper';
import Settings from '../../shared/Settings';

/*
  Główna klasa zarządzająca obiektem użytkownika oraz przeciwników. Pośrednio też
  bierze udział w zarządzaniu pociskami - przez trzymanie referencji na BulletManager.
  Ważne pola:
    messageManager - obsługa pokazywania komunikatów użytkownikowi, przekazana z klasy Game
    entities - kolekcja obiektów GameEntity
    history - tablica wszystkich stanów przewidzianych przez klienta, a nie obsłużonych jeszcze przez serwer
    historyId - id ostatniego wpisu go tablicy history
    userId - socketId lokalnego użytkownika
*/
export default class PlayerManager {

  constructor(messageManager) {
    this.messageManager = messageManager;
    this.entities = new Map();
    this.inputManager = new InputManager();
    this.logicManager = new LogicManager();
    this.bulletManager = new BulletManager(this.entities, this.logicManager);
    this.connector = new Connector(this);
    this.history = [];
    this.historyId = -1;
    this.userId = null;
  }

  /*
    Metoda obsługująca zmiany stanów wszystkich obiektów.
    Początkowo aplikuje najnowsze stany z serwera i interpoluje obiekty przeciwników.
    Następnie zbiera najnowszy stan wciśniętych przycisków i wysyła je na serwer, razem z historyId potrzebnym
    w kroku uzgadniania z serwerem (metoda setValuesFromServer)
    Ostatecznie metoda przewiduje nowy stan obiektu użytkownika na podstawie wciśniętych przecisków.
    Parametry:
      -timeDiff - różnica czasu od ostatniego wywołania pętli rysowania
  */
  logic(timeDiff) {
    if(!this.userId) { //jeśli jeszcze nie przydzielono id użytkownikowi, to nie ma co pokazać
      return;
    }
    this.setValuesFromServer();
    const inputState = this.inputManager.state;

    ++this.historyId;

    this.connector.send(new MessageContent(inputState, this.historyId, timeDiff));

    this.clientPrediction(timeDiff, inputState);
  }

  render(ctx) {
    this.entities.forEach((entity) => {
      entity && entity.object.render(ctx);
    });
    this.bulletManager.render(ctx);
  }

  /*
    Metoda obsługująca krok przewidywania klienta.
    Używając ostatnio przewidzianego stanu użytkownika, aplikuje go do LogicManagera w celu obliczenia stanu najnowszego
    względem wciśniętych przycisków.
    Najnowszy stan może również zawierać informację o potrzebie stworzenia obiektu pocisku co metoda deleguje do BulletManagera.
    Następnie przewidziany zostaje stan istniejących pocisków.
    Parametry:
      -timeDiff - różnica czasu od ostatniego wywołania pętli rysowania
      -inputState - stan wciśniętych przycisków przez użytkownika
  */
  clientPrediction(timeDiff, inputState) {
    const user = this.entities.get(this.userId).object;
    const otherPlayers = LogicHelper.getLogicPlayersForClientEntities(this.entities, this.userId);
    const { bulletState, playerState } = this.logicManager.processPlayerLogic(new LogicModel(inputState, user.getState(), timeDiff, otherPlayers));
    this.bulletManager.addUserBullet(bulletState, this.userId, this.historyId);
    this.bulletManager.predictBullets(timeDiff, playerState, this.userId);
    this.updateStateLocally(playerState, timeDiff, inputState);
  }

  /*
    Metoda aktualizująca obiekt użytkownika o przewidziany stan. Poza tym umieszcza w historii stanów
    nieprzetworzonych jeszcze przez serwer stan wciśniętych przycisków potrzebny w kroku uzgadniania z serwerem.
    Parametry:
      -newPlayerState - najnowszy stan klienta przewidziany w kroku clientPrediction
      -timeDiff - różnica czasu od ostatniego wywołania pętli rysowania
      -inputState - stan wciśniętych przycisków przez użytkownika
  */
  updateStateLocally(newPlayerState, timeDiff, inputState) {
    const user = this.entities.get(this.userId).object;
    if(Settings.clientPredictionEnabled) {
      user.update(newPlayerState);
      this.history.push(new HistoryModel(inputState, this.historyId, timeDiff));
    }
  }

  /*
    Metoda obsługująca krok uzgadniania przewidywań z serwerem i uruchamiająca interpolację przeciwników.
    Uzgadnianie rozpoczyna się od pobrania najnowszego stanu klienta obliczonego przez serwer.
    Następnie metoda iteruje po tablicy History, stany już wzięte pod uwagę przez serwer są z niej usuwane,
    a reszta jest ponownie przewidywana na podstawie najnowszego stanu z serwera.
  */
  setValuesFromServer() {
    this.interpolateEntities();
    const userEntity = this.entities.get(this.userId);
    const user = userEntity.object;
    const clientData = userEntity.newestState();
    if(!clientData) {
      return;
    }
    const historyId = clientData.historyId;
    let toRemove = -1;
    let playerState = clientData.objectState.clone();
    if(historyId === null) { //skoro historia nie istnieje, to nadaj stan początkowy
      user.update(playerState);
      return;
    }
    for(let i = 0; i < this.history.length; ++i){
      const hId = this.history[i].historyId;
      if(hId <= historyId) {
        toRemove = i;
      }
      if(hId > historyId) {
        const historyValue = this.history[i];
        const otherPlayers = LogicHelper.getLogicPlayersForClientEntities(this.entities, this.userId);
        playerState = this.logicManager.processPlayerLogic(new LogicModel(historyValue.inputState, playerState, historyValue.timeDiff, otherPlayers)).playerState;
      }
    }
    this.history.splice(0, toRemove + 1);
    user.update(playerState);
  }

  interpolateEntities() {
    this.entities.forEach((entity, id) => {
      if(id !== this.userId) {
        entity.update();
      }
    });
  }

  /*
    Metoda otrzymująca kolekcję stanów wszystkich aktywnych obiektów z serwera.
    Nowe pociski obsługiwane są w BulletManagerze, a nowe stany użytkowników dopisywane są
    do odpowiednich obiektów GameEntity.
    Parametry:
      -serverDataCollection - kolekcja stanów wszystkich obiektów gry nadesłana przez serwer
  */
  getDataFromServer(serverDataCollection) {
    this.bulletManager.getBulletsDataFromServer(serverDataCollection.bullets, this.userId);
    serverDataCollection.playersStates.forEach((serverData) => {
      const entityState = serverData.clientData.objectState;
      const playerState = PlayerState.restoreState(entityState);
      if(!this.entities.has(serverData.socketId)) {
        this.entities.set(serverData.socketId, new GameEntity(new Player(playerState, playerState.health, playerState.cooldown)));
      }
      this.entities.get(serverData.socketId).add(new ClientData(playerState, serverData.clientData.timeDiff, serverData.clientData.historyId));
    }, this);
  }

  /*
    Metoda otrzymująca stan inicjujący grę, który aplikuje do obiektu użytkownika.
  */
  setInitialData(initialServerData) {
    this.userId = initialServerData.socketId;
    const serverPlayerState = initialServerData.clientData.objectState;
    const user = new Player(PlayerState.restoreState(serverPlayerState), serverPlayerState.health, serverPlayerState.cooldown);
    this.entities.set(this.userId, new GameEntity(user));
    this.messageManager.hideMessages();
  }

  /*
    Metoda usuwająca przeciwnika z kolekcji obiektów GameEntity jeśli został rozłączony z serwerem.
    Parametry:
      -id - id użytkownika do usunięcia
  */
  disconnectPlayer(id) {
    this.entities.delete(id);
  }

  /*
    Metoda obsługująca zdarzenie nawiązania połączenia z serwerem
  */
  connectionInitialized() {
    this.messageManager.showWaitingMessage();
    this.clearData();
  }

  finishGame(status) {
    this.clearData();
    this.messageManager.showStatus(status);
  }

  clearData() {
    this.entities.clear();
    this.userId = null;
    this.history = [];
    this.historyId = -1;
    this.bulletManager.clearData();
  }
}
