import BaseObject from './BaseObject';
import BulletState from '../../../shared/Models/EntityStateModels/BulletState';

/*
  Klasa reprezentująca pojedynczy pocisk
*/
export default class Bullet extends BaseObject {

  /*
    Poza standardowymi polami obiektu gry, pocisk posiada również informację
    o id użytkownika, który go wystrzelił.
  */
  constructor(objectState, userId) {
    super(objectState);
    this.userId = userId;
  }

  getState() {
    const baseState = super.getState();
    return new BulletState(baseState, this.userId);
  }
}
