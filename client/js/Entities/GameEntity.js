import Position from '../../../shared/Models/Position';
import Settings from '../../../shared/Settings';
import TimedQueue from '../../../shared/Collections/TimedQueue';

/*
  Klasa opakowująca modele pojazdów graczy.
  Ważne pola:
    object - referencja na konkretny model pojazdu gracza, lub przeciwnika
    (użyta nazwa object, ponieważ w przyszłości może opakowywać również inne modele)
    serverStateQueue - kolejka czasowa, przetrzymująca ostatnio nadesłane stany serwera
    dla opakowanego obiektu
*/
export default class GameEntity {

  constructor(object) {
    this.object = object;
    this.serverStateQueue = new TimedQueue(1500);
  }

  add(clientData) {
    this.serverStateQueue.add(clientData);
  }

  newestState() {
    return this.serverStateQueue.newest();
  }

  /*
    Metoda interpolująca stan obiektu.
    Oblicza w jakim dokładnie czasie przeszłym pokazać obiekt, następnie odnajduje
    w kolejce stanów z serwera stan, który wystąpił zaraz przed obliczonym czasem, oraz zaraz po tym czasie.
    Stan obiektu jest następnie interpolowany liniowo do odnalezionego czasu.
  */
  getInterpolatedState() {
    if(!Settings.interpolationEnabled) { //jeśli interpolacja jest wyłączona, zwróc po prostu najnowszy stan z serwera
      return this.newestState().objectState;
    }
    const lagTime = Date.now() - Settings.lag; //obliczenie czasu przeszłego do pokazania obiektu
    const {previous, next} = this.serverStateQueue.findBoundingElements(lagTime);
    if(!previous && !next) { //jeśli w kolejce nie ma jeszcze żadnego stanu, nie pokazuj nic
      return null;
    } else if(!next) { //jeśli mamy tylko jeden stan, zwróć go od razu
      return previous.clientData.objectState.clone();
    }
    const positionX = this.linearInterpolation(previous.clientData.objectState.position.x, next.clientData.objectState.position.x, previous.time, next.time, lagTime);
    const positionY = this.linearInterpolation(previous.clientData.objectState.position.y, next.clientData.objectState.position.y, previous.time, next.time, lagTime);

    const result = previous.clientData.objectState.clone();
    result.position = new Position(positionX, positionY);
    return result;
  }

  //użycie wzoru na interpolację liniową
  linearInterpolation(x0, x1, t0, t1, ct) {
    return x0 + (x1-x0) * (ct - t0) / (t1 - t0);
  }

  update() {
    const newPlayerState = this.getInterpolatedState();
    if(newPlayerState) {
      this.object.update(newPlayerState);
    }
  }
}
