import Settings from '../../shared/Settings';
import PlayerManager from './PlayerManager';
import EndingStatus from '../../shared/Models/EndingStatus';

/*
  Klasa obsługująca wysokopoziomowo rozgrywkę. Tworzy instancję PlayerManagera,
  obsługuje pętlę rysowania, oraz odpowiada za pokazywanie komunikatów użytkownikowi.
  Ważne pola:
    previousTickTime - czas ostatniego wykonania pętli rysowania
    fpsHistory - tablica przechowująca 60 ostatnich wartości różnicy czasowej między kolejnymi przerysowaniami canvasa
*/
export default class Game {

  constructor() {
    const canvas = document.querySelector('.board');
    this.ctx = canvas.getContext('2d');
    this.playerManager = new PlayerManager({showStatus: this.showStatus, showWaitingMessage: this.showWaitingMessage, hideMessages: this.hideMessages});
    this.previousTickTime = null;
    this.fpsHistory = [];
  }

  run () {
    this.tick();
  }

  /*
    Metoda obsługi pętli rysowania, dla jak najpłynniejszego ruchu używam requestAnimationFrame zamiast setTimeout
  */
  tick () {
    requestAnimationFrame(() => {
      this.tick();
    })
    const deltaTime = this.getTimeDelta();
    this.updateFpsMeter(deltaTime);
    this.logic(deltaTime);
    this.render();
  }

  //oblicz różnicę czasu między stanem obecnym a ostatnim obrotem pętli rysowania
  getTimeDelta() {
    const currentTime = Date.now();
    const previousTime = this.previousTickTime || currentTime;
    this.previousTickTime = currentTime;
    return (currentTime - previousTime) / 100; //użyj czasu w milisekundach
  }

  logic (time) {
    this.playerManager.logic(time);
  }

  render () {
    //wyczyść cały canvas, aby przygotować go do kolejnego stanu rysowania
    this.ctx.clearRect(0, 0, Settings.canvasSize.width, Settings.canvasSize.height);
    this.playerManager.render(this.ctx);
  }

  /*
    Metoda aktualizująca licznik fps. Liczy średnią arytmetyczną ostatnich maksymalnie
    60 czasów.
    Parametry:
      -deltaTime - różnica czasu od ostatniego wywołania pętli rysowania
  */
  updateFpsMeter(deltaTime) {
    this.fpsHistory.push(deltaTime);
    const length = this.fpsHistory.length;
    if(length > 60) { //jeśli mamy więcej niż 60 czasów w tablicy, to usuń najstarszy czas
      this.fpsHistory.shift();
    }
    const sum = this.fpsHistory.reduce((prev, value) => {
      return prev + value;
    }, 0)
    const fpsMeter = document.querySelector('.fpsMeter');
    fpsMeter.innerHTML = ((sum / length)*100).toFixed(2);
  }

  showWaitingMessage() {
    const waitingMessageArea = document.querySelector('.waitingMessage');
    waitingMessageArea.style.opacity = 1;
  }

  hideMessages() {
    const gameOverArea = document.querySelector('.gameOverMessage');
    const waitingMessageArea = document.querySelector('.waitingMessage');
    gameOverArea.style.opacity = 0;
    waitingMessageArea.style.opacity = 0;
  }

  showStatus(status) {
    const gameOverArea = document.querySelector('.gameOverMessage');
    if(status === EndingStatus.PLAYER_DISCONNECTED) {
      gameOverArea.style.color = 'brown';
      gameOverArea.innerHTML = 'Enemy was disconnected from the server';
    } else if(status === EndingStatus.WIN) {
      gameOverArea.style.color = 'green';
      gameOverArea.innerHTML = 'You have won';
    } else {
      gameOverArea.style.color = 'red';
      gameOverArea.innerHTML = 'You have been defeated';
    }
    gameOverArea.style.opacity = 1;
  }

}
