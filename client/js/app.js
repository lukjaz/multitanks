import $ from 'jquery';
import '../css/site.css';
import Game from './Game';
import Settings from '../../shared/Settings';

/*
  Plik wejściowy dla całej aplikacji klienckiej, podłącza główne eventy DOM i uruchamia grę.
*/
$(() => {
  initInput();
  const canvas = document.querySelector('.board');
  canvas.width = Settings.canvasSize.width;
  canvas.height = Settings.canvasSize.height;
  const play = new Game();
  play.run();
});

const initInput = () => {
  const clientPredictionCB = $('.clientPredictionCB');
  const entityInterpolationCB = $('.entityInterpolationCB');
  clientPredictionCB.prop('checked', Settings.clientPredictionEnabled);
  entityInterpolationCB.prop('checked', Settings.interpolationEnabled);
  clientPredictionCB.click((e) => {
    Settings.clientPredictionEnabled = e.target.checked;
  });
  entityInterpolationCB.click((e) => {
    Settings.interpolationEnabled = e.target.checked;
  });
}
