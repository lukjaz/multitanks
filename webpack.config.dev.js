import webpack from 'webpack';
import path from 'path';

export default {
  devtool: 'inline-source-map',
  context: path.resolve(__dirname, './client'),
  entry: {
    app: './js/app.js',
    libraries: ['jquery', 'socket.io-client']
  },

  output: {
    path: path.resolve(__dirname, '/dist'),
    publicPath: '/',
    filename: '[name].bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: { presets: ['es2015'] }
        }]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' }
    ]
  },

  plugins: [
        new webpack.optimize.CommonsChunkPlugin(
        {
            name: 'libraries',
            minChunks: function (module) {
                return module.context && module.context.indexOf('node_modules') !== -1;
            }
        })
      ]
}
