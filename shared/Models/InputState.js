//Dzięki zastosowaniu potęg dwójki, można łączyć te stany używając operatorów bitowych
const InputStateEnum = {
  NOTHING: 0,
  LEFT: 1,
  UP: 2,
  RIGHT: 4,
  DOWN: 8,
  SPACE: 16
}

export default InputStateEnum;
