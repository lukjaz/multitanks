import Room from './Models/Room';
import Settings from '../shared/Settings';

/*
  Klasa zarządzająca pokojami gier, w których znajdują się obiekty klientów i pocisków.
  Ważne pola:
    -rooms - mapa wszystkich pokoi
    -playerRoomMapping - mapa pomagająca zlokalizować pokój danego gracza
    -playerQueue - kolejka graczy oczekujących na przydzielenie pokoju
    -lastRoomId - id ostatnio stworzonego pokoju
*/
export default class RoomManager {

  constructor() {
    this.rooms = new Map();
    this.playerRoomMapping = new Map();
    this.playerQueue = [];
    this.lastRoomId = 0;
  }

  /*
    Metoda dodająca nowego klienta. Jeśli jego dodanie do kolejki oczekiwania przekroczy jej
    maksymalną wielkość, to tworzony jest nowy pokój. Dodawani są do niego wszyscy klienci z kolejki.
    Parametry:
      -player - obiekt klienta klasy Client do dodania
  */
  addPlayer(player) {
    if(this.playerQueue.length < Settings.roomSize - 1) {
      this.playerQueue.push(player);
      return null;
    } else {
      return this.createRoom(this.playerQueue.splice(0, Settings.roomSize - 1).concat(player));
    }
  }

  /*
    Metoda dodająca pocisk do pokoju. Litra "p" w id dba o unikalność konkatenacji userId z historyId.
    Parametry:
     -bullet - obiekt pocisku do dodania klasy BulletState z sekcji Shared
     -roomId - id pokoju, do którego powinien być dodany pocisk
  */
  addBullet(bullet, roomId) {
    const room = this.getRoom(roomId);
    if(room) {
      room.bullets.set(`${bullet.userId}p${bullet.historyId}`, bullet);
    }
  }

  /*
    Metoda aktualizująca stan pocisku w pokoju.
    Parametry:
      -bulletId - id pocisku do aktualizacji
      -roomId - id pokoju, w którym aktualizowany pocisk się znajduje
      -newBulletState - nowy stan pocisku, nadpisujący stan poprzedni
  */
  updateBullet(bulletId, roomId, newBulletState) {
    const room = this.getRoom(roomId);
    if(room) {
      room.bullets.set(bulletId, newBulletState);
    }
  }

  /*
    Metoda tworząca nowy pokój gry. Wszyscy przekazani gracze zostają do niego przypisani,
    oraz dodatkowo zostają zarejestrowani w pomocniczej mapie playerRoomMapping.
    Parametry:
      -players - kolekcja obiektów graczy klasy Client do przypisania do pokoju
  */
  createRoom(players) {
    const room = new Room(++this.lastRoomId, players);
    this.rooms.set(room.id, room);
    players.forEach((player) => {
      this.playerRoomMapping.set(player.socket.id, this.lastRoomId);
    });
    return room;
  }

  /*
    Metoda obsługująca usunięcie klienta z pokoju. Dodatkowo jest on również usuwany
    z pomocniczej mapy playerRoomMapping. Jeśli bez niego gra w pokoju nie będzie mogła
    być prowadzona to pokój też jest usuwany.
    Dodatkowo jeśli klient został rozłączony przed dołączeniem do pokoju, to
    metoda usuwa go z kolejki oczekiwania.
    Zwracany jest obiekt zawierający informację o usuniętych klientach jeśli gra się skończyła,
    o tym czy gra się skończyła, oraz id pokoju z którego klient został usunięty.
    Parametry:
      -socketId - id gniazda usuwanego klienta
  */

  deletePlayer(socketId) {
    const roomId = this.playerRoomMapping.get(socketId);
    const room = this.rooms.get(roomId);
    let gameEnded = false;
    if(roomId && room) {
      this.playerRoomMapping.delete(socketId);
      room.players.delete(socketId);

      if(room.players.size < 2) {
        room.players.forEach((player) => {
          this.playerRoomMapping.delete(player.socket.id);
        });
        this.rooms.delete(roomId);
        gameEnded = true;
      }
      return {players: room.players, gameEnded: gameEnded, roomId: roomId};
    }
    const index = this.playerQueue.findIndex((player) => {
      return player.socket.id === socketId;
    });
    if(index > -1) {
      this.playerQueue.splice(index,1);
    }
    return {players: null, gameEnded: true, roomId: null};
  }

  /*
    Metoda usuwająca pocisk z pokoju.
    Parametry:
      -bulletId - id pocisku do usunięcia
      -roomId - id pokoju, w którym usuwany pocisk się znajduje
  */
  deleteBullet(bulletId, roomId) {
    const room = this.rooms.get(roomId);
    if(room) {
      room.bullets.delete(bulletId);
    }
  }

  /*
    Metoda pobierająca dane klienta na podstawie jego socketId.
    Dodatkowo zwrócony jest też id pokoju w którym klient się znajduje.
    Parametry:
      -socketId - id gniazda klienta, którego szukamy w pokoju
  */
  getPlayerData(socketId) {
    const roomId = this.playerRoomMapping.get(socketId);
    const room = this.rooms.get(roomId);
    if(roomId && room) {
      const player = room.players.get(socketId);
      return {roomId: roomId, player: player};
    }
    return null;
  }

  getRoom(roomId) {
    return this.rooms.get(roomId);
  }
}
