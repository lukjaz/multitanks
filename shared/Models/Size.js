export default class Size {

  constructor(width, height) {
    this.width = width;
    this.height = height;
  }

  equals(size) {
    return this.width === size.width && this.height === size.height;
  }

  clone() {
    return new Size(this.width, this.height);
  }
}
