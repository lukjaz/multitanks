import express from 'express';
import server from 'http';

import path from 'path';
import open from 'open';
import webpack from 'webpack';
import config from '../webpack.config.dev';
import GameManager from './GameManager';

const compiler = webpack(config);
const app = express();
const http = server.Server(app);
const port = process.env.PORT || 3000;
const gameManager = new GameManager(http);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/views/index.html'));
});

gameManager.run();

http.listen(port, (error) => {
  if(error) {
    console.log(error);
  } else {
    open(`http://localhost:${port}`);
  }
});
