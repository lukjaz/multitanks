import Queue from './Queue';

/*
  Rozbudowanie mojej implementacji kolejki o funkcjonalność automatycznego usuwania z niej
  elementów będących w niej dłużej niż zadany w parametrze konstruktora czas.
  Ważne pola:
    -timeBoundary - maksymalny czas pozostawania elementu w kolejce
*/

export default class TimedQueue extends Queue {

  constructor(timeBoundary) {
    super();
    this.timeBoundary = timeBoundary;
  }

  add(clientData) {
    const time = Date.now();
    super.add(new TimedElement(clientData, time));
    this.updateQueue();
  }

  peek() {
    const result = super.peek();
    return result.clientData;
  }

  pop() {
    const result = super.pop();
    return result.clientData;
  }

  /*
    Metoda pobierająca najnowszy element z kolejki
  */
  newest() {
    this.updateQueue();
    return this.last ? this.last.data.clientData : null;
  }

  /*
    Metoda znajdująca element dodany najbliżej czasu przekazanego w konstruktorze.
    W tym momencie nie jest używana, jednak byłaby potrzebna przy technice "Lag Compensation".
    Parametry:
      -time - wartość czasu, dla której szukamy elementu najbliższego w kolejce
  */
  findClosest(time) {
    let it = last;
    let difference = null;
    while(it) {
      const diff = Math.abs(it.data.time - time);
      if(!difference || diff < difference) {
        difference = diff;
      } else if (diff > difference) { //jeśli różnica czasu jest większa niż poprzednia różnica, oznacza to, że zaczynamy oddalać się od elementu szukanego
        return it.prev.data.clientData;
      }
      it = it.next;
    }
  }

  /*
    Metoda wyszukująca dwa elementy w kolejce: jeden dodany wcześniej niż czas podany w konstruktorze
    a drugi później. Potrzebne przy interpolacji obiektów.
    Parametry:
      -time - wartość czasu, dla której szukamy elementu wcześniejszego i późniejszego
  */
  findBoundingElements(time) {
    let it = this.last;
    if(!it) {
      return {next: null, previous: null};
    }
    while(it) {
      if(!it.next) {
        return {previous: it.data, next: null};
      }
      if(it.data.time >= time && it.next.data.time <= time) {
        return {next: it.data, previous: it.next.data};
      }
      it = it.next;
    }
    return {previous: it.last.data, next: null};
  }

  /*
    Metoda sprawdzająca czy najwcześniej dodane elementy nadal spełniają warunek czasowy.
    Jeśli nie, to są z niej usuwane.
  */
  updateQueue() {
    const time = Date.now();
    while(this.first && time - this.first.data.time > this.timeBoundary) {
      if(this.first === this.last) {
        break;
      }
      this.pop();
    }
  }
}

/*
  Klasa opisująca element kolejki czasowej.
  Ważne pola:
    -time - czas dodania do kolejki
*/
class TimedElement {
  constructor(clientData, time) {
    this.clientData = clientData;
    this.time = time;
  }
}
