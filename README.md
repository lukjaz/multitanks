# MultiTanks

Web game which presents bidirectional communication via websockets.

## Getting Started

These instructions will help run the project on the local machine.

### Prerequisites

Libraries needed to run the project:

```
Newest version of Npm
Newest version of Node.js
```

### Installing

Open console, navigate to the project's root, and enter command:

```
npm install
```

### Running the game

When all dependecies are installed type:

```
npm start
```

Which will start the web server and open browser's tab with first client connected.

To connect another client, simply open new tab with the same address as the first one.

## Gameplay

Tanks can be moved using arrow keys. Shots are triggered by space button.

Each hit takes away 40 health, and if health equals 0, players is defeated.

By changing state of checkboxes at the bottom, user is able to observe changes in gameplay when one of web game techniques is disabled.

### Changing settings

File Settings.js contains main game settings. There is an ability to change tanks' and bullets' sizes and speed.
There is also an ability to change number of players assigned to each game room.

## Built With
* [Node.js](https://nodejs.org/en/)- Javascript server runtime.
* [Socket.io](https://socket.io/)- Websocket library for Node.js.
* [Webpack](https://webpack.github.io/)- Javascript model bundler and dependency resolver.

## Author

* **Łukasz Jaźwa**

