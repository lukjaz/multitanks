import BaseObject from './BaseObject';
import PlayerState from '../../../shared/Models/EntityStateModels/PlayerState';

/*
  Klasa reprezentująca pojazd pojedynczego gracza.
*/
export default class Player extends BaseObject {

  /*
    Poza standardowymi polami obiektu gry, pojazd gracza posiada stan swojego życia
    oraz cooldown - informację o pozostałym czasie oczekiwania na kolejny wystrzał
  */
  constructor(objectState, health, cooldown) {
    super(objectState);
    this.health = health;
    this.cooldown = cooldown;
  }

  getState() {
    const baseState = super.getState();
    return new PlayerState(baseState, this.health, this.cooldown);
  }

  update(playerState) {
    super.update(playerState);
    this.health = playerState.health;
    this.cooldown = playerState.cooldown;
  }

  render(ctx) {
    super.render(ctx);
    this.drawHealth(ctx);
    this.drawCannon(ctx);
  }

  /*
    Metoda rysuje na canvasie, mniej więcej w środku pojazdu gracza, stan jego życia.
    Parametry:
      -ctx - context canvasa
  */
  drawHealth(ctx) {
    ctx.fillStyle = 'black';
    ctx.font = '10px Arial';
    ctx.fillText(this.health, this.position.x + this.size.width / 2 - 8, this.position.y + this.size.height / 2);
  }

  /*
    Metoda rysuje działo pojazdu gracza, położenie działa jest ściśle powiązane z
    obecną rotacją (angle) pojazdu.
    Parametry:
    -ctx - context canvasa
  */
  drawCannon(ctx) {
    ctx.fillStyle = 'yellow';
    let x = this.position.x;
    let y = this.position.y;
    let width = this.size.width;
    let height = this.size.height;
    let dimension;
    switch(this.angle) {
      case 0:
        ctx.fillRect(x + width / 2 - 2, y, 4, 6);
        break;
      case 90:
        ctx.fillRect(x + width - 6, y + height / 2 - 2, 6, 4);
        break;
      case 180:
        ctx.fillRect(x + width / 2 - 2, y + height - 6, 4, 6);
        break;
      case 270:
        ctx.fillRect(x, y + height / 2 - 2, 6, 4);
        break;
    }
  }
}
