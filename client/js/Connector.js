import io from 'socket.io-client';

/*
  Klasa zajmująca się komunikacją z serwerem.
  Ważne pola:
    socket - gniazdo Socket.io, przez które przesyłamy komunikaty
    playerManager - referencja na PlayerManager
*/
export default class Connector {

  constructor(playerManager) {
    this.socket = io();
    this.listen();
    this.playerManager = playerManager;
  }

  listen() {
    //nasłuchiwanie na nowe stany propagowane z serwera do wszystkich klientów
    this.socket.on('new state', (serverDataCollection) => {
      this.playerManager.getDataFromServer(serverDataCollection);
    });

    //nasłuchiwanie na informacje o rozłączeniu któregoś z przeciwników
    this.socket.on('user disconnected', (id) => {
      this.playerManager.disconnectPlayer(id);
    });

    //zainicjowanie połączenia z serwerem
    this.socket.on('connection', () => {
      this.playerManager.connectionInitialized();
    });

    this.socket.on('game ended', (status) => {
      this.playerManager.finishGame(status);
    });

    //nasłuchowanie na informację o stworzeniu nowego pokoju i rozpoczęciu rozgrywki
    this.socket.on('game started', (initialServerData) => {
      this.playerManager.setInitialData(initialServerData);
    });
  }

  /*
    Metoda wysyłająca najnowszy stan wciśniętych przycisków na serwer.
  */
  send(state) {
    this.socket.emit('state changed', state);
  }
}
