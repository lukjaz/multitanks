import ObjectState from './ObjectState';
import Settings from '../../Settings';

export default class PlayerState extends ObjectState {

  constructor(objectState, health, cooldown) {
    super(objectState.color, objectState.size, objectState.position, objectState.speed, objectState.angle);
    this.health = health === undefined || health === null ? Settings.health : health;
    this.cooldown = cooldown || 0;
  }

  clone() {
    const baseResult = super.clone();
    return new PlayerState(baseResult, this.health, this.cooldown);
  }

  static restoreState(playerState) {
    const restoredBase = ObjectState.restoreState(playerState);
    return new PlayerState(restoredBase, playerState.health, playerState.cooldown);
  }
}
