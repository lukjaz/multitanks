export default class LogicModel {

  constructor(inputState, objectState, timeDiff, playerStates) {
    this.inputState = inputState;
    this.objectState = objectState;
    this.timeDiff = timeDiff;
    this.playerStates = playerStates;
  }
}
