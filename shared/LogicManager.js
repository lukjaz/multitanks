import InputState from '../shared/Models/InputState';
import Settings from './Settings';
import Position from './Models/Position';
import Size from './Models/Size';
import PlayerState from './Models/EntityStateModels/PlayerState';
import ObjectState from './Models/EntityStateModels/ObjectState';
import BulletState from './Models/EntityStateModels/BulletState';

/*
  Klasa odpowiadająca za obliczanie kolejnych stanów obiektów gry na podstawie stanów wcześniejszych,
  różniczy czasowej od ostatnich obliczeń, oraz, dla pojazdów, najnowszego stanu wciśniętych przycisków przez użytkownika.
  Dzięki uzależnieniu postępu zmiany stanu od różnicy czasowej, LogicManager uniezależnia obliczenia od
  współczynnika ilości klatek na sekundę (który mógłby być różny dla każdego klienta).
*/
export default class LogicManager {

  /*
    Metoda obliczająca nowy stan dla pojazdu użytkownika. Poza zmianą pozycji obsługuje
    zmianę kierunku jazdy, oraz kolizje pojazdu z brzegiem ekranu i innymi pojazdami.
    Ponadto sprawdza, czy została wciśnięta spacja i jeśli tak, to razem z nowym stanem pojazdu,
    zwraca stan nowego pocisku do dodania na ekran.
    Parametry:
      -logicModel - model klasy LogicModel
  */
  processPlayerLogic(logicModel) {
    const inputState = logicModel.inputState;
    const timeDiff = logicModel.timeDiff;
    const playerState = logicModel.objectState;
    let potentialX = playerState.position.x;
    let potentialY = playerState.position.y;
    //obsługa licznika czasu od ostatniego wystrzału
    playerState.cooldown = playerState.cooldown - timeDiff < 0 ? 0 : playerState.cooldown - timeDiff;

    if ((inputState & InputState.LEFT) === InputState.LEFT){
      potentialX -= playerState.speed * timeDiff;
    } else if((inputState & InputState.RIGHT) === InputState.RIGHT){
      potentialX += playerState.speed * timeDiff;
    }
    if((inputState & InputState.UP) === InputState.UP){
      potentialY -= playerState.speed * timeDiff;
    } else if ((inputState & InputState.DOWN) === InputState.DOWN) {
      potentialY += playerState.speed * timeDiff;
    }

    const newAngle = this.getAngle(inputState, playerState);

    let { position } = this.boundaryCollision(potentialX, potentialY, playerState);
    position = this.entitiesCollision({position: position, size: playerState.size}, logicModel.playerStates);

    const newState = playerState.clone();
    newState.position = position;
    newState.angle = newAngle;

    const bulletState = this.tryCreateBullet(newState, inputState);

    return {bulletState: bulletState, playerState: newState};
  }

  tryCreateBullet(playerState, inputState) {
    //twórz pocisk tylko jeśli została wciśnięta spacja, a od ostatniego wystrzału minęło wystarczająco dużo czasu
    if((inputState & InputState.SPACE) === InputState.SPACE && playerState.cooldown === 0) {
      playerState.cooldown = Settings.playerCooldown;
      return this.getBulletInitialState(playerState);
    }

    return null;
  }

  /*
    Metoda zwraca stan początkowy tworzonego pocisku. Jego pozycją startową jest pozycja lufy pojazdu
    przesunięta wektorowo, aby nie nakładała się z pojazdem.
    Parametry:
      -playerState - stan obiektu gracza, który stworzył pocisk
  */
  getBulletInitialState(playerState) {
    let position;
    let size;
    const x = playerState.position.x;
    const y = playerState.position.y;
    const width = playerState.size.width;
    const height = playerState.size.height;
    switch(playerState.angle) {
      case(0):
        position = new Position(x + width/2 - 2, y - 6);
        size = new Size(2, 6);
        break;
      case(90):
        position = new Position(x + width + 6, y + height / 2 - 2);
        size = new Size(6, 2);
        break;
      case(180):
        position = new Position(x + width/2 -2, y + height + 6);
        size = new Size(2, 6);
        break;
      case(270):
        position = new Position(x - 6, y + height / 2 - 2);
        size = new Size(6, 2);
        break;
    }
    return new BulletState(new ObjectState('black', size, position, Settings.bulletSpeed, playerState.angle));
  }

  /*
    Metoda aktualizująca stan pocisku. Postęp ruchu obliczany jest podobnie jak dla pojazdu,
    poza oczywiście braniem pod uwagę wciśniętych przycisków przez użytkownika.
    Sprawdzana jest również możliwa kolizja z brzegiem ekranu, oraz z pojazdami graczy.
    Parametry:
      -logicModel - obiekt klasy LogicModel
  */
  processBulletLogic(logicModel) {
    const timeDiff = logicModel.timeDiff;
    const objectState = logicModel.objectState;
    const angle = objectState.angle;
    let potentialX = objectState.position.x;
    let potentialY = objectState.position.y;
    switch(angle) {
      case(0):
        potentialY -= objectState.speed * timeDiff;
        break;
      case(90):
        potentialX += objectState.speed * timeDiff;
        break;
      case(180):
        potentialY += objectState.speed * timeDiff;
        break;
      case(270):
        potentialX -= objectState.speed * timeDiff;
        break;
    }

    const {collisionDetected, position} = this.boundaryCollision(potentialX, potentialY, objectState);
    if(collisionDetected) {
      return null;
    }

    const result = this.bulletsCollision({position: position, size: objectState.size}, logicModel.playerStates);

    const newState = objectState.clone();
    newState.position = position;
    return {bulletState: newState, collisionDetected: result.collisionDetected, playerState: {userId: result.userId, health: result.health}};
  }

  /*
    Metoda sprawdzająca kolizję obiektu gry z jednym z brzegów ekranu.
    Parametry:
      -potentialX - przewidziana pozycja x obiektu
      -potentialY - przewidziana pozycja y obiektu
      -objectState - stan obiektu dla, którego sprawdzamy kolizję
  */
  boundaryCollision(potentialX, potentialY, objectState) {
    let collisionDetected = false;
    if(potentialX < 0) {
      potentialX = 0;
      collisionDetected = true;
    } else if(potentialX + objectState.size.width > Settings.canvasSize.width) {
      potentialX = Settings.canvasSize.width - objectState.size.width;
      collisionDetected = true;
    }
    if(potentialY < 0) {
      potentialY = 0;
      collisionDetected = true;
    } else if(potentialY + objectState.size.height > Settings.canvasSize.height) {
      potentialY = Settings.canvasSize.height - objectState.size.height;
      collisionDetected = true;
    }

    return {collisionDetected: collisionDetected, position: new Position(potentialX, potentialY)};
  }

  /*
    Metoda sprawdzająca kolizję pojazdu z resztą pojazdów graczy.
    Parametry:
      -playerState - stan pojazdu gracza, dla którego liczymy kolizję
      -otherPlayerStates - stan reszty pojazdów graczy biorących udział w grze
  */
  entitiesCollision(playerState, otherPlayerStates) {
    otherPlayerStates.forEach((otherPlayerState) => {
      const { position } = this.checkCollision(playerState, otherPlayerState);
      playerState.position = position;
    });

    return playerState.position;
  }

  /*
    Metoda sprawdzająca kolizję pocisku z pojazdami graczy. Jeśli kolizja została zarejestrowana,
    poza informacją o kolizji zwracana jest też informacja o id użytkownika trafionego,
    oraz pomniejszona wartość jego stanu życia.
    Parametry:
      -bulletState - stan pocisku, dla którego sprawdzamy kolizję
      -playerStates - stan pojazdów graczy uczestniczących w grze
  */
  bulletsCollision(bulletState, playerStates) {
    const playerStatesLength = playerStates.length;
    let damagedPlayerState;
    let collisionDetected = false;
    for(let i = 0; i < playerStatesLength; ++i) {
      const playerState = playerStates[i];
      const result = this.checkCollision(bulletState, playerState)
      if(result.collisionDetected) {
        playerState.health -= 40;
        damagedPlayerState = playerState;
        collisionDetected = result.collisionDetected;
        break;
      }
    }
    return {health: collisionDetected ? damagedPlayerState.health: null, userId: collisionDetected ? damagedPlayerState.id: null, collisionDetected: collisionDetected};
  }

  /*
    Metoda sprawdzająca kolizję między dwoma obiektami świata gry,
    zwraca informację o wystąpieniu kolizji, oraz nową pozycję obiektu A
    Parametry:
      objectStateA, objectStateB - stany obiektów, dla których sprawdzamy czy wystąpiła między nimi kolizja
  */
  checkCollision(objectStateA, objectStateB) {
    let collisionDetected = false;
    let positionX = objectStateA.position.x;
    let positionY = objectStateA.position.y;
    const pLeft = objectStateA.position.x;
    const pTop = objectStateA.position.y;
    const pRight = objectStateA.position.x + objectStateA.size.width;
    const pBottom = objectStateA.position.y + objectStateA.size.height;
    const oLeft = objectStateB.position.x;
    const oTop = objectStateB.position.y;
    const oRight = objectStateB.position.x + objectStateB.size.width;
    const oBottom = objectStateB.position.y + objectStateB.size.height;

    if(pLeft < oRight && pRight > oLeft) {
      if(pTop < oBottom && pBottom > oTop) {
        collisionDetected = true;
        let x, y;
        if(Math.abs(pRight - oLeft) < Math.abs(oRight - pLeft)) {
          x = -(pRight - oLeft);
        } else {
          x = oRight - pLeft;
        }
        if(Math.abs(pBottom - oTop) < Math.abs(oBottom - pTop)) {
          y = -(pBottom - oTop);
        } else {
          y = oBottom - pTop;
        }

        if(Math.abs(x) < Math.abs(y)) {
          positionX += x;
        } else {
          positionY += y;
        }
      }
    }
    return {collisionDetected: collisionDetected, position: new Position(positionX, positionY)};
  }

  /*
    Metoda zwracająca rotację pojazdu.
    Parametry:
      -inputState - stan wciśniętych przycisków przez klienta
      -playerState - stan pojazdu użytkownika, dla którego liczona jest rotacja
  */
  getAngle(inputState, playerState) {
    switch(inputState){
      case InputState.RIGHT:
        return 90;
      case InputState.DOWN:
        return 180;
      case InputState.LEFT:
        return 270;
      case InputState.UP:
        return 0;
      default:
        return playerState.angle;
    }
  }
}
