/*
  Klasa reprezentująca wiadomości wysłaną na serwer od klienta.
  Ważne pola:
    -messageContent - zawartość wiadomości opisana klasą MessageContent z sekcji Shared
    -socketId - id gniazda użytkownika wysyłającego wiadomość
    -time - czas otrzymania wiadomości
*/
export default class Message {

  constructor(messageContent, socketId, time) {
    this.content = messageContent;
    this.socketId = socketId;
    this.time = time;
  }
}
