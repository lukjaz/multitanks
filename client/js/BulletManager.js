import LogicModel from '../../shared/Models/LogicModel';
import Bullet from './Entities/Bullet';
import BulletState from '../../shared/Models/EntityStateModels/BulletState';
import ClientData from '../../shared/Models/ClientData';
import LogicHelper from '../../shared/LogicHelper';

/*
  Klasa obsługująca wszystkie przyciski widoczne na ekranie.
  Ważne pola:
    bullets - mapa widocznych pocisków (id to konkatenacja id użytkownika, oraz id wpisu do historii
    stworzonego podczas tworzenia pocisku)
    entities - referencja na kolekcję elementów GameEntity, potrzebna do wyliczania kolizji pocisków z pojazdami
    logicManager - referencja na klasę LogicManager, dzięki której możliwe jest obliczanie kolejnych pozycji pocisku
*/
export default class BulletManager {
  constructor(entities, logicManager) {
    this.bullets = new Map();
    this.entities = entities;
    this.logicManager = logicManager;
  }

  render(ctx) {
    this.bullets.forEach((bullet) => {
      bullet.render(ctx);
    });
  }

  /*
    Metoda przewidująca kolejne pozycje pocisków na podstawie upływu czasu od ostatnich obliczeń,
    przewidzianego stanu pojazdu użytkownika, oraz ostatnich stanów przeciwników.
    Parametry:
      timediff - różnica czasu od ostatnięj pętli rysowania
      playerState - najnowszy stan obiektu lokalnego użytkownika
      userId - id lokalnego klienta
  */
  predictBullets(timeDiff, playerState, userId) {
    const bulletsToRemove = [];
    const otherPlayers = LogicHelper.getLogicPlayersForClientEntities(this.entities, userId); //pobranie stanów wszystkich przeciwników
    //do kolekcji stanów przeciwników dodajemy przetransformowany najnowszy stan użytkownika
    otherPlayers.push({position: playerState.position.clone(), size: playerState.size.clone(), id: userId, health: playerState.health});
    this.bullets.forEach((bullet, id) => {
      const currentBulletState = bullet.getState();
      const result = this.logicManager.processBulletLogic(new LogicModel(null, currentBulletState, timeDiff, otherPlayers));
      if(!result || result.collisionDetected) { //jeśli pocisk został wyleciał z planszy, lub zderzył się z pojazdem, usuń go
        bulletsToRemove.push(id);
      }
      if(result) {
        bullet.update(result.bulletState);
      }
    });
    bulletsToRemove.forEach((id) => {
      this.bullets.delete(id);
    });
  }

  /*
    Metoda dodająca nowy pocisk do kolekcji dla obecnego użytkownika.
    Parametry:
      -bulletState - najnowszy stan pocisku
      -userId - id lokalnego klienta
      -historyId - id wpisu do tablicy history w PlayerManagerze, dla którego tworzony jest pocisk
  */
  addUserBullet(bulletState, userId, historyId) {
    if(bulletState) {
      //klucz ma w środku literę 'p' aby zapewnić unikalność konkatenacji userId z historyId
      this.bullets.set(`${userId}p${historyId}`, new Bullet(bulletState, userId));
    }
  }

  /*
    Metoda dodająca nowe pociski przeciwników z kolekcji pocisków przysłanych z serwera.
    Parametry:
      -serverBullets - stan pocisków na serwerze
      -userId - id lokalnego klienta
  */
  getBulletsDataFromServer(serverBullets, userId) {
    serverBullets.forEach((serverData) => {
      const bulletId = serverData.id;
      const bulletState = BulletState.restoreState(serverData.bulletState);
      if(!this.bullets.has(bulletId) && bulletState.userId !== userId) { //dodaj tylko te pociski, których jeszcze nie renderujemy, oraz nie dodawaj pocisków użytkownika
        this.bullets.set(bulletId, new Bullet(bulletState, bulletState.userId, bulletState.historyId));
      }
    });
  }

  /*
    Metoda usuwająca wszystkie pociski, których id znajdują się w przekazanej tablicy.
  */
  removeBullets(idArr) {
    idArr.forEach((id) => {
      this.bullets.delete(id);
    })
  }

  /*
    Metoda czyszcząca całą kolekcję pocisków
  */
  clearData() {
    this.bullets.clear();
  }
}
