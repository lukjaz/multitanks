import ObjectState from '../../../shared/Models/EntityStateModels/ObjectState';

/*
  Klasa bazowa dla głównych obiektów gry
*/
export default class BaseObject {

  constructor(objectState) {
    this.update(objectState);
  }

  update(newObjectState) {
      this.position = newObjectState.position;
      this.speed = newObjectState.speed;
      this.angle = newObjectState.angle;
      this.color = newObjectState.color;
      this.size = newObjectState.size;
  }

  getState() {
    return new ObjectState(this.color, this.size, this.position, this.speed, this.angle);
  }

  render (ctx) {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.position.x, this.position.y, this.size.width, this.size.height);
  }
}
