const EndingStatusEnum = {
  PLAYER_DISCONNECTED: 0,
  DEFEAT: 1,
  WIN: 2
}

export default EndingStatusEnum;
