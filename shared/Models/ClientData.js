export default class ClientData {

  constructor(objectState, timeDiff, historyId) {
    this.objectState = objectState;
    this.timeDiff = timeDiff;
    this.historyId = historyId;
  }
}
