import InputState from '../../shared/Models/InputState';

/*
  Klasa zbierająca informacje o najnowszym stanie wciśniętych klawiszy.
  W konstruktorze rozpoczyna nasłuchiwać na zdarzenia wciśnięcia i zwolnienia przycisku.
  Stanem podstawowym jest NOTHING - czyli nic nie jest wciśnięte.
  Dzięki użyciu operatorów bitowych możliwe jest zapamiętywanie stanu wielu przycisków jednocześnie
  wewnątrz jednej zmiennej typu liczbowego.
*/
export default class InputManager {

  constructor(player) {
      document.body.onkeydown = (e) => { this.handleKeyEvent(e); };
      document.body.onkeyup = (e) => { this.handleKeyEvent(e); };

      this.state = InputState.NOTHING;
  }

  handleKeyEvent(e) {
    let inputState;
    switch(e.keyCode) {
      case 37:
        inputState = InputState.LEFT;
        break;
      case 38:
        inputState = InputState.UP;
        break;
      case 39:
        inputState = InputState.RIGHT;
        break;
      case 40:
        inputState = InputState.DOWN;
        break;
      case 32:
        inputState = InputState.SPACE;
        break;
    }
    this.updateState(inputState, e.type);
  }

  updateState(inputState, eType) {
    if(eType === 'keydown') {
      this.state |= inputState; //jeśli przecisk został wciśnięty, zsumuj go z obecnym stanem przycisków
    } else {
      this.state = this.state & ~inputState; //jeśli przycisk został zwolniony, usuń go z obecnego stanu przycisków
    }
  }
}
