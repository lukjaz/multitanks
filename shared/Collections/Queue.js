/*
  Moja implementacja kolejki. Nie chciałem używać javascriptowej tablicy jako kolejki
  ze względu na złożoność operacji na niej.
  Kolejka posiada wskaźniki na element dodany najwcześniej oraz najpóźniej. Każdy z obiektów
  posiada wskaźniki na poprzednika i następnika.
*/
export default class Queue {
    constructor() {
        this.last = null;
        this.first = null;
    }

    add(data) {
        const element = new Element(data, null, this.last);
        if(!this.last) {
            this.last = element;
            this.first = element;
        } else {
            this.last.prev = element;
            this.last = element;
        }
    }

    /*
      Metoda zwracająca najwcześniejszy element w kolejce, nie usuwa go z niej
    */
    peek() {
        return this.first ? this.first.data : null;
    }

    /*
      Metoda zwracająca najwcześniejszy element w kolejce i usuwa go z niej
    */
    pop() {
        if(!this.first) {
            return null;
        }
        const first = this.first;
        if(this.first.prev) {
            this.first.prev.next = null;
            this.first = this.first.prev;
        } else {
            this.first = null;
            this.last = null;
        }
        return first.data;
    }

    isEmpty() {
      return this.last === null;
    }
}

class Element {
    constructor(data, prev, next) {
        this.data = data;
        this.prev = prev;
        this.next = next;
    }
}
