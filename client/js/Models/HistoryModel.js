/*
  Model, który zapisujemy w tablicy przeszłych stanów pojazdu użytkownika
  (tablica history z PlayerManagera).
  Zawiera dokładnie te dane, które wysyłamy na serwer, czyli:
  stan wciśniętych przycisków, swoje id, oraz czas jaki upłynął od ostatniego odświeżenia
  pętli rysowania.
*/
export default class HistoryModel {
  constructor(inputState, historyId, timeDiff) {
    this.inputState = inputState;
    this.historyId = historyId;
    this.timeDiff = timeDiff;
  }
}
