import Position from '../../shared/Models/Position';
import Settings from '../../shared/Settings';
import TimedQueue from '../../shared/Collections/TimedQueue';

export default class Entity {

  constructor(object) {
    this.object = object;
    this.serverStateQueue = new TimedQueue(1500);
  }

  add(clientData) {
    this.serverStateQueue.add(clientData);
  }

  newestState() {
    return this.serverStateQueue.newest();
  }

  getInterpolatedState() {
    if(!Settings.interpolationEnabled) {
      return this.newestState().objectState;
    }
    const lagTime = Date.now() - Settings.lag;
    const {previous, next} = this.serverStateQueue.findBoundingElements(lagTime);
    if(!previous && !next) {
      return null;
    } else if(!next) {
      return previous.clientData.objectState.clone();
    }
    const positionX = this.linearInterpolation(previous.clientData.objectState.position.x, next.clientData.objectState.position.x, previous.time, next.time, lagTime);
    const positionY = this.linearInterpolation(previous.clientData.objectState.position.y, next.clientData.objectState.position.y, previous.time, next.time, lagTime);

    const result = previous.clientData.objectState.clone();
    result.position = new Position(positionX, positionY);
    return result;
  }

  linearInterpolation(x0, x1, t0, t1, ct) {
    return x0 + (x1-x0) * (ct - t0) / (t1 - t0);
  }

  update() {
    const newPlayerState = this.getInterpolatedState();
    if(newPlayerState) {
      this.object.update(newPlayerState);
    }
  }
}
